﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crossword
{
    [Serializable]
    public class crswrd
    {
        public List<string> words;
        public List<string> Descr;
        public int nummax, nummax2;

        public crswrd()
        {
            words = new List<string>();
            Descr = new List<string>();

        }

        public void Compl()
        {
            nummax = 0;
            nummax2 = 1;
            for (int i = 0; i < words.Count; i++)
            {
                if (words[i].Length > words[nummax].Length)
                {
                    nummax = i;
                    nummax2 = nummax;
                }
            }
        }
        public int MaxV()
        {
            return words[nummax].Length;
        }

        public int MaxG()
        {
            return words[nummax2].Length;
        }


    }
}
