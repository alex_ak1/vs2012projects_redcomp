﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Crossword
{
    public partial class Form1 : Form
    {

        crswrd crs;
        List<int> numFC; 
        List<int> numFR;
        int midG;
        bool ready;
        List<string> messag;
        public Form1()
        {
            InitializeComponent();
            crs = new crswrd();
            numFC = new List<int>();
            numFR= new List<int>();
            messag= new List<string>();
            ready = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.ShowDialog();
            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(crswrd));
                using (var fStream = new FileStream(ofd.FileName, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    crs = (crswrd)ser.Deserialize(fStream);
                }

                dataGridView1.RowCount = crs.MaxV() + 2;
                dataGridView1.ColumnCount = crs.MaxG() * 2;
                dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

                for (int i = 0; i < dataGridView1.ColumnCount; i++)
                {
                    dataGridView1.Columns[i].Width = 30;
                }
                midG = dataGridView1.ColumnCount / 2;
                for (int i = 0; i < crs.MaxV(); i++)
                {
                    //dataGridView1.Rows[i].Cells[midG].Value = crs.words[crs.nummax][i];
                    dataGridView1.Rows[i].Cells[midG].Style.BackColor = Color.Aqua;
                }
                for (int i = 0; i < crs.words.Count; i++)
                {
                    numFC.Add(-1);
                    numFR.Add(-1);
                }


                //string copymax = crs.words[crs.nummax];

                char[] cpmx = new char[crs.MaxV()];
                for (int i = 0; i < crs.MaxV(); i++)
                {
                    cpmx[i] = crs.words[crs.nummax][i];
                }

                for (int i = 0; i < crs.words.Count; i++)
                {
                    if (i != crs.nummax)
                    {
                        for (int w = 0; w < crs.words[i].Length; w++)
                        {
                            bool t = false;
                            for (int m = 0; m < crs.MaxV(); m++)
                            {
                                if (crs.words[i][w] == cpmx[m])// && cpmx[m]!='*')
                                {
                                    numFC[i] = midG - w;
                                    numFR[i] = m;
                                    cpmx[i] = '*';
                                    t = true;
                                    break;
                                    //copymax.[m] = '*';
                                }
                            }
                            if (t)
                                break;
                        }

                        if (numFC[i] != -1)
                        {
                            dataGridView1.Rows[numFR[i]].Cells[numFC[i]].Value = (i + 1) + "";
                            for (int w = 0; w < crs.words[i].Length; w++)
                            {
                                //   dataGridView1.Rows[numFR[i]].Cells[numFC[i] + w].Value = crs.words[i][w];
                                dataGridView1.Rows[numFR[i]].Cells[numFC[i] + w].Style.BackColor = Color.Aqua;//.Value = crs.words[i][w];
                            }
                            textBox1.Text += (i + 1) + ")" + crs.Descr[i] + Environment.NewLine;
                        }

                    }
                }
                textBox1.Text += (crs.nummax + 1) + ")" + crs.Descr[crs.nummax] + Environment.NewLine;
                dataGridView1.Rows[0].Cells[midG].Value = (crs.nummax + 1) + "";
                ready = true;
            }
            catch (Exception ex)
            {
                messag.Add(ex.Message);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            AddGame adg = new AddGame();
            adg.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ready)
            {
                bool win = true;
                try
                {

                    for (int i = 0; i < crs.words.Count; i++)
                    {
                        if (i != crs.nummax)
                        {
                            for (int w = 0; w < crs.words[i].Length; w++)
                            {
                                //   dataGridView1.Rows[numFR[i]].Cells[numFC[i] + w].Value = crs.words[i][w];
                                if (Convert.ToChar(dataGridView1.Rows[numFR[i]].Cells[numFC[i] + w].Value) != crs.words[i][w])
                                {
                                    win = false;
                                    break;
                                }
                            }
                            if (!win)
                                break;
                        }
                    }
                    for (int i = 0; i < crs.MaxV(); i++)
                    {
                        //dataGridView1.Rows[i].Cells[midG].Value = crs.words[crs.nummax][i];
                        // dataGridView1.Rows[i].Cells[midG].Style.BackColor = Color.Aqua;
                        if (Convert.ToChar(dataGridView1.Rows[i].Cells[midG].Value) != crs.words[crs.nummax][i])
                        {
                            win = false;
                            break;
                        }
                    }
                    if (win)
                    {
                        MessageBox.Show("Победа!");
                    }
                    else
                    {
                        MessageBox.Show("Неправильно!");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неправильно!");
                    messag.Add(ex.Message);
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            File.WriteAllLines("Log.txt", messag);
        }
    }
}
