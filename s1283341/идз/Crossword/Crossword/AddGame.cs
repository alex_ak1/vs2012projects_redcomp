﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Crossword
{
    public partial class AddGame : Form
    {

        crswrd cr;
        public AddGame()
        {
            InitializeComponent();
            cr = new crswrd();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox2.Text!="" && textBox1.Text != "")
            {
                cr.words.Add(textBox2.Text);


                cr.Descr.Add(textBox1.Text);
            }
            Refresh_lb();
            textBox2.Text = "";
            textBox1.Text = "";
        }


        void Refresh_lb()
        {
            listBox1.Items.Clear();
            for(int i=0;i<cr.words.Count;i++)
            {
                listBox1.Items.Add(cr.words[i]);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (cr.words.Count > 2)
            {
                cr.Compl();
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.ShowDialog();
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(crswrd));
                    using (var fStream = new FileStream(sfd.FileName, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        ser.Serialize(fStream, cr);
                    }
                    this.Close();
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cr.words.RemoveAt(listBox1.SelectedIndex);
            cr.Descr.RemoveAt(listBox1.SelectedIndex);
            Refresh_lb();

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = cr.Descr[listBox1.SelectedIndex];
        }
    }
}
