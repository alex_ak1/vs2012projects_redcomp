//---------------------------------------------------------------------------

#include "polynom.h"
#include <algorithm>

//---------------------------------------------------------------------------

TPolynom& operator * ( const TPolynom& p1, float m )
{
	TPolynom *r = new TPolynom( p1 );

	*r *= m;

	return *r;
}

// �������� ������� �� x^pow
TPolynom& operator ^ ( const TPolynom& p1, int pow )
{
	TPolynom *r = new TPolynom( p1.capacity + pow );

	for( int i = 0; i <= p1.highPow(); i++ )
		r->v[i + pow] += p1.v[ i ];

	return *r;
}

TPolynom& operator * ( const TPolynom& p1, const TPolynom &p2 )
{
	TPolynom *r = new TPolynom( p1.highPow() + p2.highPow() );

	int i1, i2;

	for( i1 = 0; i1 <= p1.highPow(); i1++ )
		for( i2 = 0; i2 <= p2.highPow(); i2++ )
			r->addCoeff( i1+i2, p1.v[i1] * p2.v[i2] );

	return *r;
}

TPolynom& operator + ( const TPolynom& p1, const TPolynom &p2 )
{
	int cap = std::max( p1.capacity, p2.capacity );
	TPolynom *r = new TPolynom( p1.capacity + p2.capacity );

	for( int i = 0; i < cap; i++ )
		r->coeff( i, p1.coeff(i) + p2.coeff(i) );

	return *r;
}

TPolynom& operator - ( const TPolynom& p1, const TPolynom &p2 )
{
	TPolynom *r = new TPolynom( p1.capacity + p2.capacity );
	int cap = std::max( p1.capacity, p2.capacity );

	for( int i = 0; i < cap; i++ )
		r->coeff( i, p1.coeff(i) - p2.coeff(i) );

	return *r;
}

//---------------------------------------------------------------------------

void divmod( const TPolynom& divident, const TPolynom &divisor,
	TPolynom &quotient, TPolynom &remainder )
{
    // quotient.
    // remainder = new Polinom(dividend); // (Polinom)dividend.Clone();//�������

    int i, j;
	int divHi = divisor.highPow()+1;
	int dividentHi = divident.highPow() + 1;

	// ����������� ������� � �������
	for( i = 0; i < dividentHi; i++ )
		remainder.coeff( i, divident.coeff(i) );

	if (abs(divisor.coeff( divHi-1 ) < 0.0001))
		// ������� �� 0
		return;

	for (i = 0; i <= dividentHi - divHi; i++)
    {
		float coeff = remainder.coeff( dividentHi- i - 1 ) / divisor.coeff( divHi - 1 );
        quotient.coeff( i, coeff );

		// ������� �� ������� �������� * �����������
        for (j = 0; j < divHi; j++)
        {
			int ci = dividentHi - i - j - 1;
            remainder.addCoeff( ci, -coeff * divisor.coeff( divHi - j - 1 ) );
        }
    }

    // quotient.AutoLower = remainder.AutoLower = dividend.AutoLower | divisor.AutoLower;
}

//---------------------------------------------------------------------------

// ����� ������
bool findRoot( const TPolynom &p, float a, float b, float *res, int cnt )
{
	int i;
	float d = (b-a) / cnt, x = a, x2, c;

	// ���������� ��������, ���� ��� ����� ���� ������
	for( i = 0; i < cnt; i++ )
	{
		if (p.calc( x ) * p.calc( x + d) < 0)
			break;
		x += d;
	}

	// ���� ������ �� ����� - ������
	if (p.calc( x ) * p.calc( x + d) >= 0)
		return false;

	// � ������ ��������� ����� ���-�� ����, ���� �������� �������
	x2 = x + d;

	while (fabs(  x2-x ) > eps)
	{
		c = (x + x2) / 2;

		if (p.calc(x) * p.calc( c ) < 0)
			x2 = c;
		else
			x = c;
	}

	*res = c;
	return true;
}

#pragma package(smart_init)
