//---------------------------------------------------------------------------

#ifndef polynomH
#define polynomH
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
//---------------------------------------------------------------------------

/*
	����� �������� (����������)
	�������� ������������ ��������
*/

const float eps = 1e-6;

class TPolynom
{
public:
	float *v;
	int capacity;

	void grow( int s )
	{
		if (v == NULL)
		{
			capacity = s;
			v = (float*) malloc( sizeof(float) * s );
			clear ();
		}
		else
		{
			v = (float*) realloc( v, sizeof(float) * s );

			for( int i = capacity; i < s; i++ )
				v[i] = 0;
		}

		capacity = s;
	}
public:
	TPolynom( int pow = 10 )
	{
		v = NULL;
		grow( pow );
	}

	TPolynom( const TPolynom &other )
	{
		v = NULL;
		grow( other.capacity );

		for( int i = 0; i < other.capacity; i++ )
			v[i] = other.v[i];
	}

	void clear()
	{
		memset( v, 0, sizeof(float) * capacity );
	}

	TPolynom& operator += ( const TPolynom &other )
	{
		if (capacity < other.capacity)
			grow( other.capacity );

		for( int i = 0; i < other.capacity; i++ )
			v[i] += other.v[i];

		return *this;
	}

	TPolynom& operator -= ( const TPolynom &other )
	{
		if (capacity < other.capacity)
			grow( other.capacity );

		for( int i = 0; i < other.capacity; i++ )
			v[i] -= other.v[i];

		return *this;
	}

	TPolynom& operator *= ( float m )
	{
		for( int i = 0; i < capacity; i++ )
			v[i] *= m;

		return *this;
	}

	void ToString( char *res )
	{
		res[0] = 0;
		bool first = true;

		for( int i = capacity-1; i >= 0; i-- )
			if (fabs( v[i] ) > eps)
			{
				if ((v[i] > 0) & !first)
					sprintf( &res[ strlen(res) ], "+" );

				first = false;

				// ���� ����������� �������, �� �� ������� ���
				if ((fabs( v[i] - 1 ) > eps) | (i == 0))
					sprintf( &res[ strlen(res) ], "%.2f", v[i] );

				if (i == 1)
					sprintf( &res[ strlen(res) ], "x" );
				if (i > 1)
					sprintf( &res[ strlen(res) ], "x^%d", i );
			}
	}

	void print()
	{
		char buf[100];

		ToString(buf);

		printf( buf );
	}

	float calc( float  x ) const
	{
		/* float r = 0, m;

		for( int i = 0; i < capacity; i++ )
			r += v[i] * pow( t, i ); */

		float xx = 1, s = 0;

		for( int i = 0; i <= highPow(); i++ )
		{
			s += xx * coeff(i);
			xx *= x;
		}

		return s;
	}

	// �������� �����������
	float coeff( int pow ) const
	{
		if (pow >= capacity)
			return 0;
		return v[pow];
	}

	// ���������� ����������� (�������� �������� ������)
	void coeff( int pow, float val )
	{
		if (capacity < pow)
		{
			int t = capacity;
			while (t < pow)
				t *= 2;
			grow( t );
		}

		v[pow] = val;
	}

	// �������� � ������������ ��������
	void addCoeff( int pow, float val )
	{
		coeff( pow, coeff(pow) + val );
	}

	// ����� ������ (���������) �������
	int highPow() const
	{
		int i;

		for( i = capacity-1; i >= 0; i-- )
			if (v[i] != 0)
				return i;

		return 0;
	}
};

// ��������� 
TPolynom& operator + ( const TPolynom& p1, const TPolynom &p2 );
TPolynom& operator - ( const TPolynom& p1, const TPolynom &p2 );
TPolynom& operator * ( const TPolynom& p1, const TPolynom &p2 );
TPolynom& operator ^ ( const TPolynom& p1, int pow );
TPolynom& operator * ( const TPolynom& p1, float m );

// ������� �������
void divmod( const TPolynom& p1, const TPolynom &p2,
	TPolynom &quotient, TPolynom &remainder );

bool findRoot( const TPolynom &p, float a, float b, float *res, int cnt = 100 );

#endif


