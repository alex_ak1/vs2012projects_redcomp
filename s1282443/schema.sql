USE [master]
GO
/****** Object:  Database [taxi]    Script Date: 06/15/2018 00:45:59 ******/
CREATE DATABASE [taxi] ON  PRIMARY 
( NAME = N'taxi', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\taxi.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'taxi_log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\taxi_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [taxi].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [taxi] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [taxi] SET ANSI_NULLS OFF
GO
ALTER DATABASE [taxi] SET ANSI_PADDING OFF
GO
ALTER DATABASE [taxi] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [taxi] SET ARITHABORT OFF
GO
ALTER DATABASE [taxi] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [taxi] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [taxi] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [taxi] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [taxi] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [taxi] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [taxi] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [taxi] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [taxi] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [taxi] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [taxi] SET  READ_WRITE
GO
ALTER DATABASE [taxi] SET RECOVERY FULL
GO
ALTER DATABASE [taxi] SET  MULTI_USER
GO
if ( ((@@microsoftversion / power(2, 24) = 8) and (@@microsoftversion & 0xffff >= 760)) or 
		(@@microsoftversion / power(2, 24) >= 9) )begin 
	exec dbo.sp_dboption @dbname =  N'taxi', @optname = 'db chaining', @optvalue = 'OFF'
 end
GO
USE [taxi]
GO
/****** Object:  ForeignKey [FK_driver_car]    Script Date: 06/15/2018 00:46:03 ******/
ALTER TABLE [dbo].[driver] DROP CONSTRAINT [FK_driver_car]
GO
/****** Object:  ForeignKey [FK_orderservice_card]    Script Date: 06/15/2018 00:46:03 ******/
ALTER TABLE [dbo].[orderservice] DROP CONSTRAINT [FK_orderservice_card]
GO
/****** Object:  ForeignKey [FK_orderservice_driver]    Script Date: 06/15/2018 00:46:03 ******/
ALTER TABLE [dbo].[orderservice] DROP CONSTRAINT [FK_orderservice_driver]
GO
/****** Object:  ForeignKey [FK_orderservice_order]    Script Date: 06/15/2018 00:46:03 ******/
ALTER TABLE [dbo].[orderservice] DROP CONSTRAINT [FK_orderservice_order]
GO
/****** Object:  Table [dbo].[orderservice]    Script Date: 06/15/2018 00:46:03 ******/
ALTER TABLE [dbo].[orderservice] DROP CONSTRAINT [FK_orderservice_card]
GO
ALTER TABLE [dbo].[orderservice] DROP CONSTRAINT [FK_orderservice_driver]
GO
ALTER TABLE [dbo].[orderservice] DROP CONSTRAINT [FK_orderservice_order]
GO
DROP TABLE [dbo].[orderservice]
GO
/****** Object:  Table [dbo].[driver]    Script Date: 06/15/2018 00:46:03 ******/
ALTER TABLE [dbo].[driver] DROP CONSTRAINT [FK_driver_car]
GO
DROP TABLE [dbo].[driver]
GO
/****** Object:  Table [dbo].[order]    Script Date: 06/15/2018 00:46:03 ******/
DROP TABLE [dbo].[order]
GO
/****** Object:  Table [dbo].[car]    Script Date: 06/15/2018 00:46:03 ******/
DROP TABLE [dbo].[car]
GO
/****** Object:  Table [dbo].[card]    Script Date: 06/15/2018 00:46:03 ******/
DROP TABLE [dbo].[card]
GO
/****** Object:  Table [dbo].[card]    Script Date: 06/15/2018 00:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[card](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ncard] [varchar](20) NULL,
	[fio] [varchar](100) NULL,
	[tel] [varchar](50) NULL,
	[discount] [int] NULL,
 CONSTRAINT [PK_card] PRIMARY KEY CLUSTERED 
(
	[id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[car]    Script Date: 06/15/2018 00:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[car](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[num] [varchar](20) NULL,
	[model] [varchar](50) NULL,
 CONSTRAINT [PK_car] PRIMARY KEY CLUSTERED 
(
	[id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[order]    Script Date: 06/15/2018 00:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[order](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tel] [varchar](50) NULL,
	[dt] [datetime] NULL,
	[addr] [varchar](50) NULL,
	[region] [varchar](50) NULL,
 CONSTRAINT [PK_order] PRIMARY KEY CLUSTERED 
(
	[id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[driver]    Script Date: 06/15/2018 00:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[driver](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[car_id] [int] NULL,
	[fio] [varchar](100) NULL,
 CONSTRAINT [PK_driver] PRIMARY KEY CLUSTERED 
(
	[id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[orderservice]    Script Date: 06/15/2018 00:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orderservice](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order_id] [int] NULL,
	[driver_id] [int] NULL,
	[dt] [datetime] NULL,
	[card_id] [int] NULL,
 CONSTRAINT [PK_orderservice] PRIMARY KEY CLUSTERED 
(
	[id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_driver_car]    Script Date: 06/15/2018 00:46:03 ******/
ALTER TABLE [dbo].[driver]  WITH CHECK ADD  CONSTRAINT [FK_driver_car] FOREIGN KEY([car_id])
REFERENCES [dbo].[car] ([id])
GO
ALTER TABLE [dbo].[driver] CHECK CONSTRAINT [FK_driver_car]
GO
/****** Object:  ForeignKey [FK_orderservice_card]    Script Date: 06/15/2018 00:46:03 ******/
ALTER TABLE [dbo].[orderservice]  WITH CHECK ADD  CONSTRAINT [FK_orderservice_card] FOREIGN KEY([card_id])
REFERENCES [dbo].[card] ([id])
GO
ALTER TABLE [dbo].[orderservice] CHECK CONSTRAINT [FK_orderservice_card]
GO
/****** Object:  ForeignKey [FK_orderservice_driver]    Script Date: 06/15/2018 00:46:03 ******/
ALTER TABLE [dbo].[orderservice]  WITH CHECK ADD  CONSTRAINT [FK_orderservice_driver] FOREIGN KEY([driver_id])
REFERENCES [dbo].[driver] ([id])
GO
ALTER TABLE [dbo].[orderservice] CHECK CONSTRAINT [FK_orderservice_driver]
GO
/****** Object:  ForeignKey [FK_orderservice_order]    Script Date: 06/15/2018 00:46:03 ******/
ALTER TABLE [dbo].[orderservice]  WITH CHECK ADD  CONSTRAINT [FK_orderservice_order] FOREIGN KEY([order_id])
REFERENCES [dbo].[order] ([id])
GO
ALTER TABLE [dbo].[orderservice] CHECK CONSTRAINT [FK_orderservice_order]
GO
