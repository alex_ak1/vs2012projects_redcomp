﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace taxi
{
    public partial class Drivers : Form
    {
        public Drivers()
        {
            InitializeComponent();
        }

        private void driverBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.driverBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.taxiDataSet);

        }

        private void Drivers_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "taxiDataSet.car". При необходимости она может быть перемещена или удалена.
            this.carTableAdapter.Fill(this.taxiDataSet.car);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "taxiDataSet.driver". При необходимости она может быть перемещена или удалена.
            this.driverTableAdapter.Fill(this.taxiDataSet.driver);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "taxiDataSet.car". При необходимости она может быть перемещена или удалена.
            this.carTableAdapter.Fill(this.taxiDataSet.car);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "taxiDataSet.driver". При необходимости она может быть перемещена или удалена.
            this.driverTableAdapter.Fill(this.taxiDataSet.driver);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.driverBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.taxiDataSet);

            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void driverBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.driverBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.taxiDataSet);

        }
    }
}
