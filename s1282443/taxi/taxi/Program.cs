﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace taxi
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            StreamReader rd = new StreamReader("conn.txt");

            string host = rd.ReadLine(), db = rd.ReadLine();

            Properties.Settings.Default["taxiConnectionString"] =
                "Data Source=" + host + ";Initial Catalog=" + db + ";Integrated Security=True";

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
