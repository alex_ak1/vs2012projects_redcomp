﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace taxi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Cars cars = null;

        private void button1_Click(object sender, EventArgs e)
        {
            if (cars == null)
                cars = new Cars();

            cars.ShowDialog();

        }

        Drivers drivers = null;

        private void button2_Click(object sender, EventArgs e)
        {
            if (drivers == null)
                drivers = new Drivers();

            drivers.ShowDialog();

        }

        Card card = null;

        private void button3_Click(object sender, EventArgs e)
        {
            if (card == null)
                card = new Card();

            card.ShowDialog();
        }

        private void orderBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.orderBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.taxiDataSet);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "taxiDataSet.card". При необходимости она может быть перемещена или удалена.
            this.cardTableAdapter.Fill(this.taxiDataSet.card);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "taxiDataSet.driver". При необходимости она может быть перемещена или удалена.
            this.driverTableAdapter.Fill(this.taxiDataSet.driver);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "taxiDataSet.orderservice". При необходимости она может быть перемещена или удалена.
            this.orderserviceTableAdapter.Fill(this.taxiDataSet.orderservice);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "taxiDataSet.order". При необходимости она может быть перемещена или удалена.
            this.orderTableAdapter.Fill(this.taxiDataSet.order);

        }

        private void orderDataGridView_Leave(object sender, EventArgs e)
        {
            this.Validate();
            this.orderBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.taxiDataSet);
        }
    }
}
