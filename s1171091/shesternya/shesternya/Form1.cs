﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KompasAPI7;
using Kompas6API5;

namespace shesternya
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        KompasObject kompas = null;
        Document3D doc3d = null;

        void createCompass()
        {
            // Создаем компас
            Type t = Type.GetTypeFromProgID("KOMPAS.Application.5");
            var kt = Activator.CreateInstance(t);
            kompas = (KompasObject)kt;

            kompas.Visible = true;

            // Создаем документ
            doc3d = kompas.Document3D();
            doc3d.Create(false, true);
        }

        void cs(out double x, out double y, double a, double r)
        {
            x = Math.Cos(a) * r;
            y = Math.Sin(a) * r;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /* createCompass();

            ksPart part = (ksPart) doc3d.GetPart( (int) Kompas6Constants3D.Part_Type.pTop_Part);

            // Модель
            ksEntity entitydraw = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_sketch);

            // Создаем контур шестеренки

            // Скетч
            ksSketchDefinition sketchdef = entitydraw.GetDefinition();

            // Плоскость для скетча
            ksEntity entityPlane = part.GetDefaultEntity((int)Kompas6Constants3D.Obj3dType.o3d_planeXOY);
            sketchdef.SetPlane(entityPlane);

            // Рисуем
            entitydraw.Create();

            makeGearSketch(sketchdef);
            
            // Выдавливаем (без вращения)
            double thin = Convert.ToDouble(textBox6.Text);

            // Создаем выдавливание
            ksEntity entityExtrusion = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_baseExtrusion);
            ksBaseExtrusionDefinition extrusionDef = entityExtrusion.GetDefinition();

            // Параметры - толщина и скетч
            extrusionDef.SetSideParam(true, (int)Kompas6Constants3D.End_Type.etBlind, thin, 0, true);
            extrusionDef.SetSketch(entitydraw);

            entityExtrusion.Create(); // */

            /* ksEntity entityExtrRot = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_baseRotated);
            ksBaseRotatedDefinition extrRotDef = entityExtrRot.GetDefinition();

            extrRotDef.directionType = (int)Kompas6Constants3D.Direction_Type.dtNormal;

            double a = Convert.ToDouble(textBox1.Text);

            extrRotDef.SetSideParam(true, a);
            extrRotDef.SetSketch(entitydraw);

            entityExtrRot.Create(); // */


        }
        
        // Создаем контур шестерни
        /* private void makeGearSketch(ksSketchDefinition sketchdef)
        {
            ksDocument2D doc2d = sketchdef.BeginEdit();

            int vertc;
            double r, rh, pa, pb, a, da;

            r = Convert.ToDouble(textBox1.Text);
            rh = Convert.ToDouble(textBox2.Text);
            vertc = Convert.ToInt32(textBox3.Text);

            pa = Convert.ToDouble(textBox4.Text) / 100.0;
            pb = Convert.ToDouble(textBox5.Text) / 100.0;

            da = 2 * Math.PI / vertc;
            a = 0;

            pa = da * pa;
            pb = da * pb;

            double x, y, x2, y2;
            int i;

            for (i = 0; i < vertc; i++)
            {
                cs(out x, out y, a, r - rh);
                cs(out x2, out y2, a + pa, r);

                doc2d.ksLineSeg(x, y, x2, y2, 1);

                x = x2;
                y = y2;
                cs(out x2, out y2, a + pb, r);
                doc2d.ksLineSeg(x, y, x2, y2, 1);

                x = x2;
                y = y2;
                cs(out x2, out y2, a + pb + pa, r - rh);
                doc2d.ksLineSeg(x, y, x2, y2, 1);

                x = x2;
                y = y2;
                cs(out x2, out y2, a + da, r - rh);
                doc2d.ksLineSeg(x, y, x2, y2, 1);

                a += da;
            }

            // Центральный вал
            r = Convert.ToDouble(textBox7.Text);

            doc2d.ksCircle(0, 0, r, 1);

            // Облегчающие отверстия
            vertc = Convert.ToInt32( textBox8.Text );
            r = Convert.ToDouble(textBox9.Text);
            rh = Convert.ToDouble(textBox10.Text);

            for (i = 0; i < vertc; i++)
            {
                a = i * 2 * Math.PI / vertc;

                doc2d.ksCircle(Math.Cos(a) * rh, Math.Sin(a) * rh, r, 1);
            }

            sketchdef.EndEdit(); // *
        } // */

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        void calcAngle()
        {
            double a = 0;
            try
            {
                a = Convert.ToInt32(textBox3.Text);
                a = 360 / a;

                label14.Text = "Угловой шаг зуба: " + a.ToString();
            }
            catch (Exception ex1)
            {
                return;
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            calcAngle();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            calcAngle();
        }
        
        // Создаем контур шестерни без зубцов
        private void makeCylinderSketch(ksSketchDefinition sketchdef)
        {
            ksDocument2D doc2d = sketchdef.BeginEdit();

            int vertc, i;
            double r, rh, a;

            // Наружний край
            r = Convert.ToDouble(textBox1.Text);
            doc2d.ksCircle(0, 0, r, 1);

            // Ось
            /* r = Convert.ToDouble(textBox7.Text);
            doc2d.ksCircle(0, 0, r, 1);

            // Облегчающие отверстия
            vertc = Convert.ToInt32(textBox8.Text);
            r = Convert.ToDouble(textBox9.Text);
            rh = Convert.ToDouble(textBox10.Text);

            for (i = 0; i < vertc; i++)
            {
                a = i * 2 * Math.PI / vertc;

                doc2d.ksCircle(Math.Cos(a) * rh, Math.Sin(a) * rh, r, 1);
            } // */

            sketchdef.EndEdit(); // */
        }

        // Построить фигуру, которая вырежет зуб
        private void makeToothSketch(ksSketchDefinition sketchdef)
        {
            ksDocument2D doc2d = sketchdef.BeginEdit();

            int vertc;
            double r, rh, pa, pb, da, x, y, x2, y2;

            r = Convert.ToDouble(textBox1.Text);
            rh = Convert.ToDouble(textBox2.Text);
            vertc = Convert.ToInt32(textBox3.Text);

            pa = Convert.ToDouble(textBox4.Text) / 100.0;
            pb = Convert.ToDouble(textBox5.Text) / 100.0;

            da = 2 * Math.PI / vertc;

            pa = da * pa;
            pb = da * pb;

            cs(out x, out y, 0, r);
            cs(out x2, out y2, pa, r-rh );
            doc2d.ksLineSeg(x, y, x2, y2, 1);

            x = x2;
            y = y2;
            cs(out x2, out y2, pa + pb, r - rh);
            doc2d.ksLineSeg(x, y, x2, y2, 1);

            x = x2;
            y = y2;
            cs(out x2, out y2, pa*2 + pb, r);
            doc2d.ksLineSeg(x, y, x2, y2, 1);

            x = x2;
            y = y2;
            cs(out x2, out y2, pa * 2 + pb, r+10);
            doc2d.ksLineSeg(x, y, x2, y2, 1);

            x = x2;
            y = y2;
            cs(out x2, out y2, 0, r+10);
            doc2d.ksLineSeg(x, y, x2, y2, 1);

            x = x2;
            y = y2;
            cs(out x2, out y2, 0, r);
            doc2d.ksLineSeg(x, y, x2, y2, 1);

            sketchdef.EndEdit();
        }

        void makeSpiral(ksCylindricSpiralDefinition def)
        {
            double r, h, a;

            r = Convert.ToDouble(textBox1.Text);
            h = Convert.ToDouble(textBox6.Text) + 10;
            a = Convert.ToDouble(textBox11.Text);

            // def.heightType = 0; // по обьекту
            def.buildMode = 2; // По количеству витков и высоте
            def.diam = r * 2;
            def.height = h;
            def.turn = a / 360.0;
        }

        void makeCircleSketch(ksSketchDefinition sketchdef, double r)
        {
            ksDocument2D doc2d = sketchdef.BeginEdit();

            doc2d.ksCircle(0, 0, r, 1);

            sketchdef.EndEdit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            createCompass();

            ksPart part = (ksPart)doc3d.GetPart((int)Kompas6Constants3D.Part_Type.pTop_Part);

            ksEntity xoyPlane = part.GetDefaultEntity((int)Kompas6Constants3D.Obj3dType.o3d_planeXOY);
            ksEntity zAxis = part.GetDefaultEntity((int)Kompas6Constants3D.Obj3dType.o3d_axisOZ);

            double thin;
            // ksDocument2D doc2d;

            // Создаем контур шестеренки (без зубцов)
            ksEntity entitydraw;
            ksSketchDefinition sketchdef;

            // Наружний цилиндр 
            entitydraw = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_sketch);
            sketchdef = entitydraw.GetDefinition();

            sketchdef.SetPlane(xoyPlane);
            entitydraw.Create();
            makeCircleSketch( sketchdef, Convert.ToDouble(textBox1.Text) );

            // Выдавливаем (без вращения)
            thin = Convert.ToDouble(textBox6.Text);

            // Создаем выдавливание
            ksEntity entityExtrusion = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_baseExtrusion);
            ksBaseExtrusionDefinition extrusionDef = entityExtrusion.GetDefinition();
           
            // Параметры - толщина и скетч
            extrusionDef.SetSideParam(true, (int)Kompas6Constants3D.End_Type.etBlind, thin, 0, true);
            extrusionDef.SetSketch(entitydraw);

            entityExtrusion.Create(); // */

            // Создаем выдавливание для оси
            entitydraw = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_sketch);
            sketchdef = entitydraw.GetDefinition();

            sketchdef.SetPlane(xoyPlane);
            entitydraw.Create();
            makeCircleSketch(sketchdef, Convert.ToDouble(textBox9.Text));

            // Выдавливаем (без вращения)
            thin = Convert.ToDouble(textBox10.Text);

            // Создаем выдавливание
            ksEntity entityExtrusion2 = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_baseExtrusion);
            ksBaseExtrusionDefinition extrusionDef2 = entityExtrusion2.GetDefinition(); // */

            extrusionDef2.SetSideParam(true, (int)Kompas6Constants3D.End_Type.etBlind, thin, 0, true);
            extrusionDef2.SetSketch(entitydraw);

            entityExtrusion2.Create(); // */

            // Создаем дырку на оси
            entitydraw = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_sketch);
            sketchdef = entitydraw.GetDefinition();

            sketchdef.SetPlane(xoyPlane);
            entitydraw.Create();
            makeCircleSketch(sketchdef, Convert.ToDouble(textBox7.Text));

            // Выдавливаем (без вращения)
            // thin = Convert.ToDouble(textBox10.Text);

            // Создаем выдавливание
            ksEntity entityCutExtrusion3 = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_cutExtrusion);
            ksCutExtrusionDefinition extrusionDef3 = entityCutExtrusion3.GetDefinition(); // */

            extrusionDef3.cut = true;
            // extrusionDef3.directionType = (int) Kompas6Constants3D.Direction_Type.dtReverse;
            extrusionDef3.SetSketch(entitydraw);
            // extrusionDef3.SetThinParam(false, 0, 3 * thin);
            // extrusionDef3.SetSideParam(true, (int)Kompas6Constants3D.End_Type.etThroughAll, 3 * thin);
            ksExtrusionParam extrParam = extrusionDef3.ExtrusionParam();
            extrParam.direction = (int)Kompas6Constants3D.Direction_Type.dtBoth;
            extrParam.depthReverse = 3 * thin;

            entityCutExtrusion3.Create(); // */

            // Создаем контур зубца
            ksEntity tooth = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_sketch);
            sketchdef = tooth.GetDefinition();
            sketchdef.SetPlane(xoyPlane);

            tooth.Create();
            makeToothSketch(sketchdef);

            // Создаем направляющую спираль
            ksEntity spiralEntity = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_cylindricSpiral);
            ksCylindricSpiralDefinition spyralDef = spiralEntity.GetDefinition();

            spyralDef.SetPlane(xoyPlane);
            makeSpiral(spyralDef);
            spiralEntity.Create();

            // Выдавливание кинематическое
            ksEntity evoEntity = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_cutEvolution);
            ksCutEvolutionDefinition evoDef = evoEntity.GetDefinition();

            evoDef.cut = true;
            evoDef.SetSketch(tooth);

            ksEntityCollection col1 = evoDef.PathPartArray();

            col1.Clear();
            col1.Add(spiralEntity);

            evoEntity.Create();

            // Круговое копирование
            ksEntity circularEntity = part.NewEntity((int)Kompas6Constants3D.Obj3dType.o3d_circularCopy);
            ksCircularCopyDefinition circularDef = circularEntity.GetDefinition();

            int vertc;
            vertc = Convert.ToInt32(textBox3.Text);

            circularDef.count1 = 1;
            circularDef.SetAxis(zAxis);
            circularDef.SetCopyParamAlongDir(vertc, 360 / vertc, false, false);

            col1 = circularDef.GetOperationArray();
            col1.Clear();
            col1.Add(evoEntity);

            circularEntity.Create ();         
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
