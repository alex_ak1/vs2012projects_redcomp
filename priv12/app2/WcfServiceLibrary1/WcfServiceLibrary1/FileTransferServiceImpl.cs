﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

namespace WcfServiceLibrary1
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service1" в коде и файле конфигурации.
    public class FileTransferServiceImpl : IFileTransferService
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        string _UploadFolder = "";

        public string UploadFolder
        {
            get
            {
                return _UploadFolder;
            }
            set
            {
                _UploadFolder = value;
            }
        }

        public void UploadFile(RemoteFileInfo request)
        {
            FileStream targetStream = null;
            Stream sourceStream = request.FileByteStream;


            string filePath = Path.Combine(UploadFolder, Path.GetFileName(request.FileName));

            using (targetStream = new FileStream(filePath, FileMode.Create,
                                                  FileAccess.Write, FileShare.None))
            {
                // sourceStream.CopyTo(targetStream);
                const int bufferLen = 1 << 16;
                byte[] buffer = new byte[bufferLen];
                int count = 0;

                while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    targetStream.Write(buffer, 0, count);

                targetStream.Close();
                sourceStream.Close();
            }
        }

        public RemoteFileInfo DownloadFile(DownloadRequest request)
        {
            RemoteFileInfo result = new RemoteFileInfo();
            try
            {
                string filePath = System.IO.Path.Combine(UploadFolder, request.FileName);
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);

                if (!fileInfo.Exists)
                    throw new System.IO.FileNotFoundException("File not found",
                                                              request.FileName);

                System.IO.FileStream stream = new System.IO.FileStream(filePath,
                          System.IO.FileMode.Open, System.IO.FileAccess.Read);

                result.FileName = request.FileName;
                result.Length = fileInfo.Length;
                result.FileByteStream = stream;
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
