﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfServiceLibrary1;

namespace clientapp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Connecting");
            IFileTransferService client = new FileTransferServiceImpl();
            
            string sPath = "D:\\t1.txt";

            System.IO.FileInfo fileInfo =
                new System.IO.FileInfo(sPath);

            RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();

            using (System.IO.FileStream stream =
                   new System.IO.FileStream(sPath,
                   System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                Console.WriteLine("Uploading");

                uploadRequestInfo.FileName = sPath;
                uploadRequestInfo.Length = fileInfo.Length;
                uploadRequestInfo.FileByteStream = stream;
                client.UploadFile(uploadRequestInfo);
            }

            Console.WriteLine("Done");

            Console.ReadLine();
        }
    }
}
