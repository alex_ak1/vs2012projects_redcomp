﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using WcfServiceLibrary1;

namespace clientapp1
{
    class Program
    {
        static void Main(string[] args)
        {
            (new Program()).Exec();
        }

        void Exec()
        {
            Console.WriteLine("Connecting");
            IFileTransferService client = new FileTransferServiceImpl();
            
            string sPath = "D:\\t1.txt";

            System.IO.FileInfo fileInfo =
                new System.IO.FileInfo(sPath);

            RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();

            using (System.IO.FileStream stream =
                   new System.IO.FileStream(sPath,
                   System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                Console.WriteLine("Uploading");

                uploadRequestInfo.FileName = sPath;
                uploadRequestInfo.Length = fileInfo.Length;
                uploadRequestInfo.FileByteStream = stream;
                client.UploadFile(uploadRequestInfo);
            }

            string fname;

            // Получаем список
            Console.WriteLine( "File list: " );

            client.BeginFileEnum();
            while (client.NextFileEnum(out fname))
                Console.WriteLine("  " + fname);

            /* Console.ReadLine();
            return; // */

            client.slowResponce = true;

            // Грузим весь список файлов в темп
            Console.WriteLine("Loading files:");
            client.BeginFileEnum();
            List<Thread> thr_list = new List<Thread>();

            while (client.NextFileEnum(out fname))
            {
                var thr = new Thread(new ParameterizedThreadStart(Load));
                thr.Start(client);

                thr_list.Add(thr);
            }

            Console.WriteLine("Waiting end");
            foreach (var thr in thr_list)
                thr.Join();

            Console.WriteLine("Done");

            Console.ReadLine();
        }

        void Load( object obj )
        {
            IFileTransferService client = (IFileTransferService) obj; 
            string fname = client.currFileEnum;

            if (fname == "") return;

            Console.WriteLine("  " + fname);
            DownloadRequest req = new DownloadRequest();
            req.FileName = fname;

            var resp = client.DownloadFile(req);

            var fout = new System.IO.FileStream("d:\\temp\\" + req.FileName,
                System.IO.FileMode.Create, System.IO.FileAccess.Write);

            resp.FileByteStream.CopyTo(fout);

            fout.Close();

            Console.WriteLine("  " + fname + " done" );
        }
    }
}
