!  Console1.f90 
!
!  FUNCTIONS:
!  Console1 - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: Console1
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program Console1

    implicit none

    ! Variables

    ! Body of Console1
    !integer(2) m    
    !integer(2) k
    !integer(2) n
    !integer(2) p
    
    !.m=2**7-1
    !k=m/2**2
    !n=k+mod(k,10)
    !p=k+n/2
    
    print *, "hello"

    end program Console1

