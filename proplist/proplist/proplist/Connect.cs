﻿using System;
using Extensibility;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;
using System.Resources;
using System.Reflection;
using System.Globalization;
using System.Windows.Forms;

namespace proplist
{
	/// <summary>Объект для реализации надстройки.</summary>
	/// <seealso class='IDTExtensibility2' />
	public class Connect : IDTExtensibility2, IDTCommandTarget
	{
		/// <summary>Реализация конструктора для объекта надстройки. Помещайте в этот метод ваш код инициализации.</summary>
		public Connect()
		{
		}

		/// <summary>Реализация метода OnConnection интерфейса IDTExtensibility2. Получение уведомления о загрузке надстройки.</summary>
		/// <param term='application'>Корневой объект ведущего приложения.</param>
		/// <param term='connectMode'>Описание способа загрузки надстройки.</param>
		/// <param term='addInInst'>Объект, представляющий данную надстройку.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
		{
			_applicationObject = (DTE2)application;
			_addInInstance = (AddIn)addInInst;
			if(connectMode == ext_ConnectMode.ext_cm_UISetup)
			{
				object []contextGUIDS = new object[] { };
				Commands2 commands = (Commands2)_applicationObject.Commands;
				string toolsMenuName = "Tools";

				//Поместите команду в меню "Сервис".
				//Найдите панель команд MenuBar, являющуюся панелью команд верхнего уровня, в которой содержатся все основные элементы меню:
				Microsoft.VisualStudio.CommandBars.CommandBar menuBarCommandBar = ((Microsoft.VisualStudio.CommandBars.CommandBars)_applicationObject.CommandBars)["MenuBar"];

				//Найдите панель команд "Сервис" на панели MenuBar:
				CommandBarControl toolsControl = menuBarCommandBar.Controls[toolsMenuName];
				CommandBarPopup toolsPopup = (CommandBarPopup)toolsControl;

				//Этот блок try/catch можно продублировать, если требуется добавить несколько команд для обработки надстройкой,
				//  следует обязательно обновить метод QueryStatus/Exec, чтобы он включал в себя новые команды.
				try
				{
					//Добавить команду в коллекцию Commands:
					Command command = commands.AddNamedCommand2(_addInInstance, "proplist", "proplist", "Executes the command for proplist", true, 59, ref contextGUIDS, (int)vsCommandStatus.vsCommandStatusSupported+(int)vsCommandStatus.vsCommandStatusEnabled, (int)vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeButton);

					//Добавить элемент управления для команды в меню "Сервис":
					if((command != null) && (toolsPopup != null))
					{
						command.AddControl(toolsPopup.CommandBar, 1);
					}
				}
				catch(System.ArgumentException)
				{
					//В нашем случае исключение, вероятно, вызвано тем, что команда с этим именем
					//  уже существует. В таком случае нет необходимости повторно создавать команду и можно 
                    //  спокойно игнорировать данное исключение.
				}
			}
		}

		/// <summary>Реализация метода OnDisconnection интерфейса IDTExtensibility2. Получение уведомления о выгрузке надстройки.</summary>
		/// <param term='disconnectMode'>Описание способа выгрузки надстройки.</param>
		/// <param term='custom'>Массив параметров, специфичных для ведущего приложения.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnDisconnection(ext_DisconnectMode disconnectMode, ref Array custom)
		{
		}

		/// <summary>Реализация метода OnAddInsUpdate интерфейса IDTExtensibility2. Получение уведомления при изменении коллекции надстройки.</summary>
		/// <param term='custom'>Массив параметров, специфичных для ведущего приложения.</param>
		/// <seealso class='IDTExtensibility2' />		
		public void OnAddInsUpdate(ref Array custom)
		{
		}

		/// <summary>Реализация метода OnStartupComplete интерфейса IDTExtensibility2. Получение уведомления о завершении загрузки ведущего приложения.</summary>
		/// <param term='custom'>Массив параметров, специфичных для ведущего приложения.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnStartupComplete(ref Array custom)
		{
		}

		/// <summary>Реализация метода OnBeginShutdown интерфейса IDTExtensibility2. Получение уведомления о выгрузке ведущего приложения.</summary>
		/// <param term='custom'>Массив параметров, специфичных для ведущего приложения.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnBeginShutdown(ref Array custom)
		{
		}
		
		/// <summary>Реализация метода QueryStatus интерфейса IDTCommandTarget. Этот метод вызывается при обновлении доступности команды</summary>
		/// <param term='commandName'>Имя команды, для которой определяется состояние.</param>
		/// <param term='neededText'>Текст, необходимый для команды.</param>
		/// <param term='status'>Состояние команды в интерфейсе пользователя.</param>
		/// <param term='commandText'>Текст, запрошенный параметром NeededText.</param>
		/// <seealso class='Exec' />
		public void QueryStatus(string commandName, vsCommandStatusTextWanted neededText, ref vsCommandStatus status, ref object commandText)
		{
			if(neededText == vsCommandStatusTextWanted.vsCommandStatusTextWantedNone)
			{
				if(commandName == "proplist.Connect.proplist")
				{
					status = (vsCommandStatus)vsCommandStatus.vsCommandStatusSupported|vsCommandStatus.vsCommandStatusEnabled;
					return;
				}
			}
		}

		/// <summary>Реализация метода Exec интерфейса IDTCommandTarget. Этот метод вызывается при запуске команды.</summary>
		/// <param term='commandName'>Имя выполняемой команды.</param>
		/// <param term='executeOption'>Описывает, как должна выполняться команда.</param>
		/// <param term='varIn'>Параметры, передаваемые из вызывающего объекта в обработчик команд.</param>
		/// <param term='varOut'>Параметры, передаваемые обработчиком команды в вызывающий объект.</param>
		/// <param term='handled'>Сообщение вызывающему объекту об обработке или не обработке команды.</param>
		/// <seealso class='Exec' />
		public void Exec(string commandName, vsCommandExecOption executeOption, ref object varIn, ref object varOut, ref bool handled)
		{
			handled = false;
			if(executeOption == vsCommandExecOption.vsCommandExecOptionDoDefault)
			{
				if(commandName == "proplist.Connect.proplist")
				{
                    MessageBox.Show("exec called");
                    var win = _applicationObject.ActiveWindow;

                    win.
					handled = true;
					return;
				}
			}
		}
		private DTE2 _applicationObject;
		private AddIn _addInInstance;
	}
}