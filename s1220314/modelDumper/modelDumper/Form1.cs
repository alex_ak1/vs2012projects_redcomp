﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KompasAPI7;
using Kompas6API5;
using Kompas6Constants3D;

namespace modelDumper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            textBox1.Text = openFileDialog1.FileName;
        }

        KompasObject kompas = null;
        Document3D doc3d;

        void createCompass()
        {
            // Создаем компас
            Type t = Type.GetTypeFromProgID("KOMPAS.Application.5");
            var kt = Activator.CreateInstance(t);
            kompas = (KompasObject)kt;

            kompas.Visible = true;

            doc3d = kompas.Document3D();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            createCompass();

            doc3d.Open(textBox1.Text);

            ksPart part = doc3d.GetPart((int)Part_Type.pTop_Part);

            ksBodyCollection bodyCol = part.BodyCollection();

            int cnt = bodyCol.GetCount();

            textBox2.AppendText("Тел: " + cnt + "\n");

            for (int i = 0; i < 10; i++)
            {
                ksBody body = bodyCol.GetByIndex(i);
                if (body == null) continue;

                ksFaceCollection faceCol = body.FaceCollection();
                // ksFeature feature = body.GetFeature();

                string s = "";

                // if (feature != null) s = feature.name;

                textBox2.AppendText("Тело " + i + " граней " + faceCol.GetCount() + " " + s + "\n" );

                for (int i2 = 0; i2 < Math.Min( faceCol.GetCount(), 20 ); i2++)
                {
                    ksFaceDefinition face = faceCol.GetByIndex(i2);

                    ksEdgeCollection edgeCol = face.EdgeCollection();

                    s = "  грань " + i2 + ": ";

                    for (int i3 = 0; i3 < edgeCol.GetCount(); i3++)
                    {
                        ksEdgeDefinition edge = edgeCol.GetByIndex(i3);

                        ksVertexDefinition vdef;
                        double x, y, z;

                        vdef = edge.GetVertex(true);
                        vdef.GetPoint(out x, out y, out z);

                        s = s + string.Format("({0:F0}; {1:F0}; {2:F0})-", x, y, z);

                        vdef = edge.GetVertex(false);
                        vdef.GetPoint(out x, out y, out z);
                         
                        s = s + string.Format("({0:F0}; {1:F0}; {2:F0})  ", x, y, z);
                    }

                    textBox2.AppendText(s + "\n");
                }
                if (faceCol.GetCount() > 20)
                    textBox2.AppendText("Выводим только первые 20 граней");
            }

        }
    }
}