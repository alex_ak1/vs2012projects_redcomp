﻿Imports System.IO
Public Class Form1
    Dim n As Integer        ' Размерность квадратной матрицы
    Dim m(,), m1(,) As Double  'Исходная и конечные матрицы размером n*n
    Dim flag As Boolean     ' Ответ есть
    Dim toch = 2               ' Количество десятичных знаков после запятой при выводе результатов        
    Dim sFL As New SaveFileDialog
    Dim sLast As String

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Randomize()
        flag = False
        n = Convert.ToInt32(TextBox1.Text)
        ReDim m(n, n)
    End Sub

    ' Сгенерировать новую случайную матрицу, вывести ее в Richtextbox1
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Call Gen()
        MatrixToRichTextbox(m, RichTextBox1)
    End Sub

    ' Выводит квадратную матрицу ma в RichTextBox rtb.
    Sub MatrixToRichTextbox(ByVal ma(,) As Double, ByVal rtb As RichTextBox)
        rtb.Clear()                         ' Очистка текстбокса
        For i As Integer = 0 To n - 1       ' i - строка
            For j As Integer = 0 To n - 1
                rtb.AppendText(String.Format("{0,7}", Math.Round(ma(i, j), toch)) & Chr(9))
                '    Convert.ToString(Math.Round(ma(i, j), toch)) & Chr(9)) ' Добавление элементов матрицы через табуляция
            Next
            rtb.AppendText(vbCrLf) ' Добавление символа конца строки
        Next
    End Sub

    ' Генерация матрицы случайных чисел
    Private Sub Gen()
        Dim max, min As Integer
        If T2.Text = "" Or T3.Text = "" Then
            m = Nothing
            MsgBox("Недостаточно данных для генерации матрицы! Пожалуйста, введите данные в поля.", MsgBoxStyle.Critical, "Ошибка")
        Else
            n = Val(TextBox1.Text)
            ReDim m(n, n)   ' Переобьявить квадратную матрицы m, стереть старое содержимое.
            max = Val(T3.Text)
            min = Val(T2.Text)
            If max < min Or max = min Then
                MsgBox("Максимальное значение меньше минимального. Введите правильные значения: max > min  ", 16 + 0, "Ошибка")
            Else
                For i As Integer = 0 To n - 1
                    For j As Integer = 0 To n - 1
                        m(i, j) = CInt(Int((max - min + 1) * Rnd() + min))
                    Next
                Next
            End If
        End If
    End Sub

    ' Размер матрицы - применить (рисует поля для элементов матрицы)
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If (Not IsNumeric(TextBox1.Text)) Or (TextBox1.Text.IndexOf(",") >= 0) Or Convert.ToInt32(TextBox1.Text) < 1 Then
            MsgBox("Введен недопустимый размер")
            Exit Sub
        End If
        Dim old_n = n
        n = Convert.ToInt32(TextBox1.Text)
        If n > 100 Then
            MsgBox("Указано слишком большое значение для размера матрицы")
            Exit Sub
        End If
        MyReDimPreserve(m, old_n, n) ' Изменение размера 2мерной матрицы с сохранением значений элементов с old_n, n.
        MatrixToRichTextbox(m, RichTextBox1)    ' вывод исходной матрицы
    End Sub

    ' Изменение размера 2мерной матрицы m, с old_n*old_n, на new_n*new_n, с сохранением значений элементов.
    Sub MyReDimPreserve(ByRef ma(,) As Double, ByVal old_n As Integer, ByVal new_n As Integer)
        Dim buf(new_n, new_n) As Double
        If old_n > new_n Then old_n = new_n
        For i As Integer = 0 To old_n - 1
            For j As Integer = 0 To old_n - 1
                buf(i, j) = ma(i, j)
            Next
        Next
        ReDim m(new_n, new_n)
        For i As Integer = 0 To new_n - 1
            For j As Integer = 0 To new_n - 1
                ma(i, j) = buf(i, j)
            Next
        Next
    End Sub

    ' Кнопка Решение - 1) ВВОДИТ RTB1 в матрицу m, 2) выводит решение в m1 и RTB2.
    ' 119. Для заданной квадратной матрицы сформировать одномерный массив из ее диагональных элементов.
    ' Найти след матрицы, суммируя элементы одномерного массива. 
    ' Преобразовать исходную матрицу по правилу: четные строки разделить на полученное значение, 
    ' нечетные оставить без изменения.
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        TextNToMatrix(RichTextBox1.Text)    ' 1) ВВОДИТ RTB1 в матрицу m
        ReDim m1(n, n)  ' 2) Изменить размер итоговой матрицы, стереть содержимое
        TextBox3.Text = ""  ' Элементы главной диагонали - след матрицы
        Dim sum As Double   ' Сумма элементов главной диагонали
        sum = 0
        For i As Integer = 0 To n - 1
            sum = sum + m(i, i)     ' m(i, i) - формула главной диагонали
            TextBox3.Text = (TextBox3.Text & Math.Round(m(i, i), toch).ToString() & " ")
        Next
        TextBox2.Text = Convert.ToString(Math.Round(sum, toch))   ' Сумма элементов главной диагонали
        flag = True                             ' Ответ есть
        If (sum <> 0) Then
            For i As Integer = 0 To n - 1       ' i - строка
                For j As Integer = 0 To n - 1
                    If i Mod 2 = 1 Then         ' если строки четные (1-я строка, индекс 0 - нечетная)
                        m1(i, j) = m(i, j) / sum
                    Else
                        m1(i, j) = m(i, j)
                    End If
                Next
            Next
        Else
            MsgBox("След равен 0.Новая матрица не будет создана")

            flag = False        ' Ответа нет
            RichTextBox2.Clear()
            Exit Sub
        End If
        MatrixToRichTextbox(m1, RichTextBox2)   ' Вывод ответа
    End Sub

    ' Сохранение результата в файл, включая диагональ Textbox3 и след Textbox4.
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If TextBox3.Text <> "" Then
            sFL.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*"
            sFL.OverwritePrompt = False
            Select Case sFL.ShowDialog()
                Case DialogResult.OK
                    If RichTextBox1.Text = sLast Then
                        MessageBox.Show("В файле обнаруженна идентичная запись.Повторное сохранение не производится!", "Предупреждение!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    If RichTextBox1.Text <> sLast Then
                        Try
                            Dim fileWriter As StreamWriter
                            fileWriter = New StreamWriter(sFL.FileName, True) ' append = True - с дозаписью
                            fileWriter.WriteLine("Исходная матрица")
                            For i As Integer = 0 To n - 1
                                For j As Integer = 0 To n - 1
                                    fileWriter.Write(String.Format("{0,7} ", Math.Round(m(i, j), toch)))
                                Next
                                fileWriter.WriteLine("")
                            Next
                            fileWriter.WriteLine("Массив диагонали")
                            fileWriter.WriteLine(TextBox3.Text)
                            fileWriter.WriteLine("След матрицы")
                            fileWriter.WriteLine(TextBox2.Text)
                            If flag Then
                                fileWriter.WriteLine("Новая матрица")
                                For i As Integer = 0 To n - 1
                                    For j As Integer = 0 To n - 1
                                        fileWriter.Write(String.Format("{0,7} ", Math.Round(m1(i, j), toch)))
                                    Next
                                    fileWriter.WriteLine("")
                                Next
                                fileWriter.WriteLine("")
                            Else
                                fileWriter.WriteLine("Новой матрицы не существует")
                            End If
                            fileWriter.Close()
                        Catch ex As Exception
                            MsgBox("Проблемы с текстовым файлом " & vbCrLf & ex.Message)
                        End Try
                            End if
            End Select
        Else
            MessageBox.Show("Нечего сохранять!", "Предупреждение!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        sLast = RichTextBox1.Text
    End Sub
    ' Загрузка матрицы из файла
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim OFD As New OpenFileDialog With {.Filter = "Текстовые файлы|*.txt"} ' Диалог открытия файла
        OFD.InitialDirectory = My.Computer.FileSystem.CurrentDirectory  ' Открывать в текущей директории
        If OFD.ShowDialog = Windows.Forms.DialogResult.OK Then  ' Если нажата ОК
            Try                     ' Попытаемся загрузить матрицу из текстового файла
                Dim txt = My.Computer.FileSystem.ReadAllText(OFD.FileName)  ' Считать весь файл в txt
                TextNToMatrix(txt)
            Catch ex As Exception   ' Если считать матрицу не удалось, возникла ошибка
                MsgBox("Проблемы с текстовым файлом " & vbCrLf & ex.Message)
            End Try
        End If
        'MatrixToRichTextbox(m, RichTextBox1)    ' вывод исходной матрицы
    End Sub

    ' Загрузка текста в матрицу, первая строка = размер матрицы.
    Sub TextNToMatrix(ByVal txt As String)
        Dim i, i2, w As Integer
        txt = txt.Replace(".", ",")
        Dim sp = {Chr(9), Chr(10), Chr(13), Chr(32)} ' табуляция, новая строка, пробел - разделители
        ' Dim x() = txt.Split(sp, StringSplitOptions.RemoveEmptyEntries) ' разбить txt на числа
        Dim x() = txt.Split({Chr(10), Chr(13)}, StringSplitOptions.RemoveEmptyEntries)

        If x.Length = 0 Then
            MsgBox("Текст пуст")
            Exit Sub
        End If

        For i = 0 To (x.Length - 1)    ' цикл заполнения матрицы
            Dim x2() = x(i).Split({Chr(9), Chr(32)}, StringSplitOptions.RemoveEmptyEntries)

            If (i = 0) Then
                w = x2.Length
                n = w
                ReDim m(w - 1, x.Length - 1)
                If (w <> x.Length) Then
                    MsgBox("Неквадратая матрица")
                    Exit Sub
                End If
            Else
                If (x2.Length <> w) Then
                    MsgBox("Неправильное количесво элементов в строке " & Str(i))
                    Exit Sub
                End If
            End If

            For i2 = 0 To w - 1
                If Not IsNumeric(x2(i2)) Then
                    MsgBox("Обнаружено нечисловое значение в строке " & Str(i))
                    Exit Sub
                End If

                m(i, i2) = Convert.ToDouble(x2(i2))  ' проверка формата текста x(i) путем конвертирования в double
            Next
        Next

        MatrixToRichTextbox(m, RichTextBox1)    ' вывод исходной матрицы
    End Sub

    ' Проверка на ввод целого в Textbox
    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TextBox1.KeyPress
        If Char.IsNumber(e.KeyChar) Then                        ' Проверка на ввод цифр
            If (e.KeyChar = "0") And (TextBox1.Text.IndexOf("0") = -1) Then ' Ввод единственного нуля
                e.Handled = False
            ElseIf e.KeyChar <> "0" And (TextBox1.Text = "0") Then  ' Ввод ненулевой цифры
                TextBox1.Text = TextBox1.Text.Replace("0", e.KeyChar)
                TextBox1.SelectionStart = TextBox1.TextLength
                e.Handled = True
            ElseIf (TextBox1.Text = "0") And e.KeyChar = "0" Then ' Ввод второго нуля при уже имеющемся
                e.Handled = True
            End If
        ElseIf Char.IsControl(e.KeyChar) Then ' Контрольные символы разрешены
            e.Handled = False
        Else
            e.Handled = True    ' Все остальные запрещены
        End If
    End Sub
    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Сброс.Click
        RichTextBox1.Clear()
        RichTextBox2.Clear()
        TextBox3.Clear()
        TextBox2.Clear()
    End Sub
    Private Sub T2_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles T2.KeyPress
        If Char.IsNumber(e.KeyChar) Then                        ' Проверка на ввод цифр
            If (e.KeyChar = "0") And (T2.Text.IndexOf("0") = 0) Then ' Ввод единственного нуля
                e.Handled = True
            ElseIf e.KeyChar <> "0" And (T2.Text = "0") Then  ' Ввод ненулевой цифры
                T2.Text = T2.Text.Replace("0", e.KeyChar)
                T2.SelectionStart = T2.TextLength
                e.Handled = True
            ElseIf (TextBox1.Text = "0") And e.KeyChar = "0" Then ' Ввод второго нуля при уже имеющемся
                e.Handled = True
            End If
        ElseIf (e.KeyChar = "-") And (T2.Text.IndexOf("-") = -1) Then ' Лидирующий минус разрешен
            T2.Text = "-" & T2.Text
            e.Handled = True
        ElseIf Char.IsControl(e.KeyChar) Then ' Контрольные символы разрешены
            e.Handled = False
        Else
            e.Handled = True    ' Все остальные запрещены
        End If
    End Sub

    Private Sub T3_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles T3.KeyPress
        If Char.IsNumber(e.KeyChar) Then                        ' Проверка на ввод цифр
            If (e.KeyChar = "0") And (T3.Text.IndexOf("0") = 0) Then ' Ввод единственного нуля
                e.Handled = True
            ElseIf e.KeyChar <> "0" And T3.Text = "0" Then  ' Ввод ненулевой цифры
                T3.Text = T3.Text.Replace("0", e.KeyChar)
                T3.SelectionStart = T3.TextLength
                e.Handled = True
            ElseIf (TextBox1.Text = "0") And e.KeyChar = "0" Then ' Ввод второго нуля при уже имеющемся
                e.Handled = True
            End If
        ElseIf (e.KeyChar = "-") And (T3.Text.IndexOf("-") = -1) Then ' Лидирующий минус разрешен
            e.Handled = True
            T3.Text = "-" & T3.Text
        ElseIf Char.IsControl(e.KeyChar) Then ' Контрольные символы разрешены
            e.Handled = False
        Else
            e.Handled = True    ' Все остальные запрещены
        End If
    End Sub

    ' ТЕкстбокс с матрицей. Не разрешены два минуса!
    Private Sub RichTextBox1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles RichTextBox1.KeyPress
        If Char.IsNumber(e.KeyChar) Then                        ' Проверка на ввод цифр
            If (e.KeyChar = "0") And (RichTextBox1.Text.IndexOf("0") = 0) Then
            ElseIf e.KeyChar <> "0" And RichTextBox1.Text = "0" Then  ' Ввод ненулевой цифры
                RichTextBox1.Text = RichTextBox1.Text.Replace("0", e.KeyChar)
                RichTextBox1.SelectionStart = RichTextBox1.TextLength
                e.Handled = False
            ElseIf (RichTextBox1.Text = "0") And e.KeyChar = "0" Then ' Ввод второго нуля при уже имеющемся
                e.Handled = True
            End If
        ElseIf (e.KeyChar = "-") Then ' Лидирующий минус разрешен: слева пробел, справа цифра
            e.Handled = minus()
        ElseIf Char.IsControl(e.KeyChar) Then ' Контрольные символы разрешены
            e.Handled = False
        ElseIf (e.KeyChar = ",") Then
            e.Handled = dot()
        ElseIf (e.KeyChar = ",") And e.KeyChar = "," Then
            e.Handled = False
        ElseIf (e.KeyChar = " ") Then
            e.Handled = False
        Else
            e.Handled = True  'Все остальные запрещены
        End If
    End Sub

    ' Лидирующий минус разрешен: слева пробел, справа цифра
    Function minus()
        If (RichTextBox1.Text(RichTextBox1.SelectionStart) <= "9" And _
                RichTextBox1.Text(RichTextBox1.SelectionStart) >= "0" And _
                RichTextBox1.Text(RichTextBox1.SelectionStart - 1) = vbTab) Then Return False Else Return True
    End Function
    Function dot()
        If RichTextBox1.Text(RichTextBox1.SelectionStart - 1) >= "0" And RichTextBox1.Text(RichTextBox1.SelectionStart) = vbTab Then
            Return False
        Else : Return True
        End If
    End Function


    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs)
    End Sub

    Private Sub Form1_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Dim w As Integer


        w = (ClientSize.Width - 3 * 8) / 2

        RichTextBox1.Left = 8
        RichTextBox1.Width = w
        RichTextBox2.Left = w + 16
        RichTextBox2.Width = w
    End Sub
End Class
